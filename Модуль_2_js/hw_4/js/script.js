'use strict';
const forms = document.querySelectorAll(".form");

const addError = (el, span, errorMessageElemClass, inputType, errorMessage) => {
  if (span.matches(`.${errorMessageElemClass}`)) {
    span.textContent = errorMessage;
  }

  if (el.matches(`[type="${inputType}"]`)) {
    el.style.borderColor = "red";
  }
};

const removeError = (el, span, errorMessageElemClass, inputType) => {
  if (span.matches(`.${errorMessageElemClass}`)) {
    span.textContent = "";
  }

  if (el.matches(`[type="${inputType}"]`)) {
    el.style.borderColor = "";
  }
};

const validateChecker = (
  el,
  span,
  inputType,
  errorMessageElemClass,
  inputTypeValue,
  validationResult,
  errorMessege,
  invalidValueErrorMessage
) => {
  span.style.color = "red";

  if (inputTypeValue === "") {

    addError(el, span, errorMessageElemClass, inputType, errorMessege);

  } else {

    removeError(el, span, errorMessageElemClass, inputType);

    if (validationResult) {

      removeError(el, span, errorMessageElemClass, inputType);

      return inputTypeValue;

    } else {

      addError(
        el,
        span,
        errorMessageElemClass,
        inputType,
        invalidValueErrorMessage
      );

    }
  }
};

const validator = (el, span, { email, password }) => {
  const customCheckbox = document.querySelector('[role="checkbox"]');
  const emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  const passwordRegex = /.{8,}/;

  let invalidMessage = `Поле обязательно для заполнения`;
  let isEmailValid = emailRegex.test(String(email).toLowerCase());
  let isPsswordValid = passwordRegex.test(password);
  let checkboxStatus = false;
  
  let validEmail = validateChecker(
    el,
    span,
    "email",
    "em-error",
    email,
    isEmailValid,
    invalidMessage,
    "Email не валидный"
  );
  let validPassword = validateChecker(
    el,
    span,
    "password",
    "pass-error",
    password,
    isPsswordValid,
    invalidMessage,
    "Пароль должен содержать как минимум 8 символов"
  );

  if (el.checked === false) {

    addError(el, span, "check-error", "checkbox", invalidMessage);

    customCheckbox.style.borderColor = "red";

  } else {

    removeError(el, span, "check-error", "checkbox");

    customCheckbox.style.borderColor = "green";

  }

  if (el.matches('[type="checkbox"]')) {

    checkboxStatus = el.checked;

  }

  return validEmail && validPassword && checkboxStatus ? true : false;
};

[...forms].forEach((elem) => {
  [...elem].forEach((inputs) => {
    if (inputs.matches(".form__input")) {
      const span = document.createElement("span");

      if (inputs.matches('[type="email"]')) {
        span.classList.add("em-error");
      }
      if (inputs.matches('[type="password"]')) {
        span.classList.add("pass-error");
      }
      if (inputs.matches('[type="checkbox"]')) {
        span.classList.add("check-error");
      }

      inputs.parentNode.insertAdjacentElement("afterend", span);

      elem.addEventListener("submit", (event) => {
        event.preventDefault();

        const formData = new FormData(elem);
        // console.log(...formData); // Why is emty?? AAAhhh ok just need Spred sintax...
        const inputValuesObj = {};
        // Or iterate formData entries for create object
        for (const val of formData.entries()) {
          inputValuesObj[val[0]] = val[1];
        }

        const isValid = validator(inputs, span, inputValuesObj);

        if (isValid) {
          console.log("inputValuesObj ", inputValuesObj);
        }
      });
    };
  });
});
