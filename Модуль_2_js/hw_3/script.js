/**
 * Запрос списка пользователей с api
 * @returns возвращает список пользователей в формате json
 */
const userList = async () => {
    const res = await fetch("https://reqres.in/api/users?per_page=12");
    const data = await res.json();
    return data;
};

/**
 * Обрабатывает полученные данные выводя результат в консоль
 * @param {*} usersData данные пользователей
 * @param {*} id для вывода номера пункта и выполнения условия соответствующего пункту
 * @param {*} title для вывода заголовка который описывает действие испольняющееся в условии
 */
const action = (usersData, id, title) => {
    console.log("-----------");
    console.log(`Пункт №${id}: ${title}`);
    console.log("-----------");
    
    if (id === 1) {
        console.log(usersData);
    }

    if (id === 2) {
        usersData.forEach((user) => console.log(user.last_name));
    }

    if (id === 3) {
        const filtredUsers = usersData.filter((users) => users.last_name[0] === "F");
                                            
        filtredUsers.forEach(( user ) => {

            for (let key in user) {
                const userInfo = `-${key}: ${user[key]}`
                console.log(userInfo);
            };

        });
    }

    if (id === 4) {
        
        const userNameArr = [];
        
        usersData.reduce((acc, item) => {
            userNameArr.push(acc)
            return acc = `${item.first_name} ${item.last_name}`;
        }, '');
        
        const usersName = userNameArr.splice(1).join(', ')
        
        console.log('Наша база содержит данные следующих пользователей: ', usersName);
    }

    if (id === 5) {
        const users = [];

        usersData.forEach((user) => users.push(user));

        for (let key in users[0]) {
            console.log(key);
        };
    }
    
};

userList().then(({ data }) => {
    const usersData = [];
    data.forEach(users => usersData.push(users));

    action(usersData, 1, 'Get all user data');
    action(usersData, 2, 'Get all user last name');
    action(usersData, 3, 'Get all user data if last name starts with F');
    action(usersData, 4, 'Output all users name in data base use Reduce method');
    action(usersData, 5, 'Get all user object keys');

});
