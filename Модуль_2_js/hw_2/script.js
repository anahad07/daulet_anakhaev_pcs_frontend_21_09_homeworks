"use strict";

const fibonacci = (

    () => {
        
        let [result, first, second] = [0, 1, 1];

        const fib = () => {
                
            [result, first, second] = [first, second, first + second];

            console.log(result);

            return result;
        };

        return fib;

    }

)();

fibonacci(); // 1
fibonacci(); // 1
fibonacci(); // 2
fibonacci(); // 3
fibonacci(); // 5
