"use strict";

let firstValue = prompt("Введите первое число ", "");
const operator = prompt("Введите введите знак", "");
let lastValue  = prompt("Введите введите второе число", "");

const isNumber = (n) => {
  return !isNaN(parseFloat(n)) && isFinite(n);
};

const isCheckZero = (checkedValue) => {
  checkedValue = +checkedValue;

  if (!checkedValue) {
    throw new Error(`Попытка деления на ${checkedValue}...`);
  }

  return checkedValue;
};

if (!firstValue) {
  console.log("Первое число не указано");
} else if (!isNumber(firstValue)) {
  console.log("Некорректный ввод чисел");
}

if (!operator) {
  console.log("Не введён знак");
}

if (!lastValue) {
  console.log("Второе число не указано");
} else if (!isNumber(lastValue)) {
  console.log("Некорректный ввод чисел");
}

firstValue = +firstValue;
lastValue = +lastValue;

switch (operator) {
  case "+":
    alert(firstValue + lastValue);
    break;

  case "-":
    alert(firstValue - lastValue);
    break;

  case "*":
    alert(firstValue * lastValue);
    break;

  case "/":
    alert(firstValue / isCheckZero(lastValue));
    break;

  default:
    console.log("Программа не поддерживает такую операцию");
}
